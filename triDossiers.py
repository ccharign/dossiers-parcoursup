#! /usr/bin/python3
# -*- coding: utf-8 -*-

from lecture_parcourSup import crée_table_APB, charge_parcours_sup
from sqlite import *
from importlib import reload
import sys

ICI = "/home/moi/git/dossiers-parcoursup/"
DONNÉES = os.path.join(ICI, "données/2021")
MPSI2021 = os.path.join(ICI, "données/2021/MPSI.csv")
AVEC_Z=os.path.join(ICI, "données/2021/avecZ.csv")
BASE = os.path.join(ICI, "données/2021/dossiers.db")


vue_tout= """
DROP VIEW IF EXISTS Tout;
CREATE VIEW Tout
AS
SELECT 

       nom, 
       prénom,

       ROUND(moyenneMP,3) AS moyenne,
       ROUND(moyenne_maths_première,2) AS maths_première,
       ROUND(moyenne_maths_term,2) AS maths_term,
       ROUND(maths_ex,2) AS maths_expertes,
       ROUND(moyenne_PC_première,2) AS PC_première,
       ROUND(moyenne_PC_term,2) AS PC_term,
       ROUND(moyenne_SI_sans_physique_première,2) AS SI_première,
       ROUND(moyenne_SI_term,2) AS SI_term ,
       ROUND(moyenne_NSI_première,2) AS NSI_première,
       ROUND(moyenne_NSI_term,2) AS NSI_term,
       ROUND((français+philo)/2.,2) AS français_philo,	
       ROUND(moyenne_LV1_première,2) AS LV1_première ,	
       ROUND(moyenne_LV1_term,2) AS LV1_term ,

       Appréciations,

       sexe,
       date_naissance,
       dossier_complet, 
       boursier,
       type_terminale,
       lycée_origine, 
       commune_lycée_origine,
       numéro_ligne
       --moyenneMP_partielle,
FROM Dossiers;
"""


COLONNES_À_GARDER = """
 numéro_ligne,
 ROUND(moyenneMP,2) AS moyenne,
 nom,
 prénom,
 date_naissance,
 ROUND(moyenne_maths_term) AS maths_term,
 ROUND(moyenne_PC_term) AS moyenne_PC_term,
 ROUND(français_philo) AS français_philo,
 ROUND(moyenne_LV1_term) AS moyenne_LV1_term,
 ROUND(moyenne_NSI_term) AS moyenne_NSI_term,
 ROUND(moyenne_SI_term) AS moyenne_SI_term,
 Appréciations AS Appréciations_sur_20,
 maths_ex,
 dossier_complet,
 type_terminale,
 lycée_origine
"""

def list_vers_csv(tab, fichier):
    fichier.write(";".join(map(str,tab))+"\n")

def réinitBase(chemin=MPSI2021, année=2021, debug=0):
    
    print(f"Création de la base {BASE}\n")
    crée_table_APB(BASE, ajout_numéro_ligne=True,debug=debug-1)
    print(f"Chargement du csv {chemin}\n")
    charge_parcours_sup(chemin, BASE, année=année, debug=debug-1)

    print("Création de la vue finale\n")
    conn=sqlite3.connect(BASE)
    cu=conn.cursor()
    execute_str_sql(vue_tout, cu)
    conn.commit()


    # trier selon la note et récupérer le rang
    cu.execute(f"""
    SELECT * FROM Tout
        WHERE type_Terminale = "Série Générale"
              AND dossier_complet
              AND moyenne is not NULL
     ORDER BY moyenne DESC
     """)
    champs = ["rang", "Z"]+récupère_champs(cu)
    rang = 1
    dossiers_normaux=[]
    for ligne in cu:
        if rang <= 300 : z=1
        elif rang > 800 : z=-1
        else :z=0
        dossiers_normaux.append([rang,z]+list(ligne))
        rang+=1

    #Dossiers incomplets ou pas TG:
    cu.execute(f"""
    SELECT * FROM Tout
        WHERE type_Terminale!="Série Générale"
              OR NOT dossier_complet
              OR moyenne is NULL
     """)
    dossiers_incomplets=[]
    for ligne in cu:
        dossiers_incomplets.append([-1,-10]+list(ligne))

    tout = dossiers_normaux + dossiers_incomplets
    # On remet dans l'ordre initial, celui du fichier excel de l'administration
    case_numéro_ligne_initial = champs.index("numéro_ligne")
    tout.sort( key=lambda l: int(l[case_numéro_ligne_initial]) )

    sortie=open(os.path.join(DONNÉES,"dossiers_triés.csv"),"w")
    list_vers_csv(champs, sortie)
    for ligne in tout:
        list_vers_csv(ligne, sortie)
    sortie.close()



    
À_Appliquer={"Z classé":float, "Z de base":int, "numéro_ligne":int}

def applique_à_dico(d):
    for c, f in À_Appliquer.items():
        d[c] = f(d[c])

        
def csv_vers_dicos(chemin, sép = ";"):
    """ Entrée : chemin, adresse d'un fichier csv.
                 sép, le séparateur de champs
        Précondition : la première ligne donne les numéros de colonne
        Sortie : itérateur sur les lignes du csv. Chaque élément renvoyé est un dictionnaire nom de col -> valeur."""
    entrée = open(chemin)
    clefs = entrée.readline().strip().split(sép)
    res = []
    for ligne in entrée:
        d = {clefs[i] : val for i, val in enumerate(ligne.strip().split(sép))}
        applique_à_dico(d)
        res.append(d)
    entrée.close()
    return clefs, res

CORRECTION_VILLE = {"Tarbes":-0.5, "Lourdes": -3, "Lannemezan":-3,"Bagnères-de-Bigorre":-3, "Argelès-Gazost":-3, "Saint-Jean-Pied-de-Port":-3, "Saint-Jean-de-Luz": -3, "Pau":-3, "Orthez":-3, "Oloron-Sainte-Marie":-3, "Nay":-3, "Mourenx":-3, "Montardon":-3, "Lescar":-3, "Igon":-3}

def corrige_géographie(dossiers):
    """ Annule le bonus géographique qui avait été mis dans Z"""
    for d in dossiers:
        if d["Z de base"]==0:
            d["Z classé"] += CORRECTION_VILLE.get(d["commune_lycée_origine"], 0)


def moyenne_et_variance_Z(lot):
    m = sum( d["Z classé"] for d in lot) / len(lot)
    v = sum( (d["Z classé"]-m)**2 for d in lot ) / len(lot)
    return m, v


SÉPARATIONS = [0,248, 500, 737, 965, 1100,1310]#Première et (dernière ligne+1) inclues
def normalise_Z(dossiers):
    """ Ramène à 0 la moyenne de chaque jury pour la colonne Z"""

    for i in range(len(SÉPARATIONS)-1):
        lot = [ d for d in dossiers[SÉPARATIONS[i]: SÉPARATIONS[i+1]] if d["Z de base"]==0  ]
        m, v = moyenne_et_variance_Z(lot)
        σ=v**.5
        print(f"Moyenne du lot {i} : {m}. Écart-type : {σ}")
        for d in lot:
            d["Z classé"] = (d["Z classé"]-m)/σ




def list_vers_csv(tab, fichier):
    fichier.write(";".join(map(str,tab))+"\n")
    
def dicos_vers_csv(t, champs, chemin, sép=";"):
    """
    t : tableau de dictionnaires
    champs : tableau des noms de colonnes (càd clé dans les dicos)
    chemin : adresse du fichier de sortie."""

    sortie=open(chemin,"w")
    list_vers_csv(champs, sortie)
    for ligne in t :
        list_vers_csv( [ligne[c] for c in champs], sortie)
    sortie.close()

    
def ajoute_rang(dicos):
    """ Entrée : le tab des tableaux, triés par moyenne décroissante.
       Effet : remplis le champ Rang.
    """
    r=1
    for d in dicos :
        d["Rang"]=r
        r+=1


def traitement_final(chemin = AVEC_Z):

    champs, dossiers = csv_vers_dicos(chemin)
    
    corrige_géographie(dossiers)

    normalise_Z(dossiers)

    dossiers_normaux = [ d for d in dossiers if d["Z classé"]!=-10 and d["Rang"]==""]
    print(f"{len(dossiers_normaux)} dossiers normaux")
    dossiers_refusés = [ d for d in dossiers if d["Z classé"]==-10 ]
    print(f"{len(dossiers_refusés)} dossiers refusés")
    dossiers_spéciaux = [ d for d in dossiers if d["Rang"]!="" ]
    print(f"{len(dossiers_spéciaux)} dossiers spéciaux")

    assert len(dossiers_normaux) + len(dossiers_refusés) + len(dossiers_spéciaux) == len(dossiers)
    
    dossiers_normaux.sort(key = lambda d : -d["Z classé"]/2 - float(d["moyenne"])) # tri par moyenne décroissante
    
    #Insertion des dossiers spéciaux
    for d in dossiers_spéciaux:
        dossiers_normaux.insert( int(d["Rang"]) , d)

    ajoute_rang(dossiers_normaux)

    res = sorted( dossiers_normaux + dossiers_refusés, key = lambda d:d["numéro_ligne"] )
    
    dicos_vers_csv(res, champs, os.path.join(DONNÉES, "final.csv" ))

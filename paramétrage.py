# -*- coding:utf-8 -*-


#  Ce script contient les paramètres pour traiter les csv de parcourSup
#
#    * les dictionnaires NORMALISE et normalise_matière servent à renommer les colonnes du csv
#      les clefs présentes dans normalise_matière ont un traitement supplémentaire : création des colonnes effectif et classement associées
#      NB : Les colonnes qui ne sont ni dans NORMALISE ni dans normalise_matière seront ignorées
#
#    * le dictionnaire TRAITEMENTAPB définit la fonction à appliquer aux éléments de la colonne (typiquement int_ou_rien, ou float_ou_rien)
#
#    * la liste d'association COLONNES_À_CRÉER définit les colonnes à créer. Typiquement des moyennes. ( str, dico -> float) List
#
#    * TOUTES_LES_COLS est la list des colonnes. Sert pour initialiser la base.
#
#    * SUFFIXES (str List) : ce sont des noms de colonnes qui seront suffixés à toutes les colonnes suivantes. A priori il s'agit de ["Terminale", "Première"]



from sqlite import en_ASCII_majuscule,en_ASCII_minuscule_Initiale






#############################################################
####             Normalisation des colonnes              ####
#############################################################

    
### Paramétrage pour le traitement de la table APB (parcoursSup aussi) ###
#    - Pour commencer, on identifie les colonnes utiles, et on leur associe un nom de colonne dans notre table, grâce à normalise_matière et normalise_autres.
#    Les colonnes de normalise_matière sont suivie d'une colonne classement puis d'une colonne Effectif, qui seront postfixées par le nom de la matière
#    - Un traitement est effectué sur les colonnes indiquées dans TRAITEMENTAPB
#    - Certaines nouvelles colonnes sont créées à partir de celles de base (moyennes par exemple). On utilisera ici la liste d'association nouvelles_colonnes ( (int, dico->'a) list )

# rema : Les tables Parcoursup ont des noms de colonne en majuscule, la table créée n'en aura pas.

# Associe un nom plus cours, et surtout sans espace ni caractère spécial à chaque matière; ce nom servira de nom de colonne dans la table APB.

MATIÈRES = ["LV1", "maths", "maths_com", "maths_ex", "PC", "SI", "SI_sans_physique", "NSI", "philo", "français"]

NORMALISE={
    "Classement Barthou":"classement_Barthou",
    "Nom":"nom",
    "Prénom":"prénom",
    "Sexe":"sexe",
    "Date de naissance":"date_naissance",
    "Profil du candidat":"profil",
    "Boursier":"boursier",
    "Classe":"classe",
    "Classement":"dossier_complet",
    "Entrée en seconde":"entrée_seconde",
    "Profil du candidat":"profil",
    "Classe":"classe_terminale",
    "Note EAF écrit":"EAF_écrit",
    "Note EAF oral":"EAF_oral",
    "UAI établissement":"UAI_lycée_origine",
    "Libellé établissement":"lycée_origine",
    "Commune établissement":"commune_lycée_origine",
    "Département établissement":"dpt_lycée_origine",
    "Méthode de travail":"méthode_travail",
    "Autonomie":"autonomie",
    "Capacité à s'investir":"capacité_investissement",
    "Autres éléments d'appréciation":"autres_éléments",
    "Niveau de la classe":"niveau_classe",
    "Avis sur la capacité à réussir":"avis_CE",
    "Numéro":"numéro",
    "Année scolaire":"année_scolaire",
    "Série":"type_",

    #Matières non répétées
    "Note à l'épreuve de Français écrit":"français_écrit",
    "Note à l'épreuve de Français oral":"français_oral",
    "Mathématiques Expertes (note)":"maths_ex",
    "Classement (Mathématiques Expertes)" : "maths_ex_classement_absolu",
    "Effectif (Mathématiques Expertes)" : "maths_ex_effectif",
    "Philosophie (note)":"philo",
    "Classement (Philosophie)":"philo_classement_absolu",
    "Effectif (Philosophie)":"philo_effectif"
    }


#### Création automatique des entrées de NORMALISE pour les matières ayant plusieurs colonnes. ####
#### Création en simultané de TOUTES_LES_COLS ####



# Pour ces matières, on a deux entrées par trimestre : moyenne et moyenne de la classe.
MATIÈRES_RÉPÉTÉES = [
    ("Sciences de l'ingénieur et sciences physiques","SI"),
    ("Sciences de l'ingénieur", "SI_sans_physique"),
    ("Langue vivante A","LV1"),
    ("Mathématiques Spécialité", "maths"),
    ("Physique-Chimie Spécialité", "PC"),
    ("Numérique et Sciences Informatiques","NSI")
]


SUFFIXES = ["Terminale", "Première"]

def suite_dico_et_nv_cols(matières, suffixes):
    """ Renvoie les entrées à rajouter dans NORMALISE (str->str dico) et les nouvelles colonnes dans la base (str List)"""
    dico={}
    cols=[]
    for trimestre in range(1,3):
        dico.update(
            {
                f"Moyenne candidat en {mat_long} Trimestre {trimestre}" : f"{mat}_t{trimestre}" for (mat_long,mat) in MATIÈRES_RÉPÉTÉES
            })
        dico.update(
            {
                f"Moyenne classe en {mat_long} Trimestre {trimestre}" : f"{mat}_t{trimestre}_classe" for (mat_long,mat) in MATIÈRES_RÉPÉTÉES
            })
    for s in suffixes:
        for (_,v) in dico.items():
            cols.append(v+s)
    return dico, cols

d, c = suite_dico_et_nv_cols(MATIÈRES_RÉPÉTÉES, SUFFIXES)
TOUTES_LES_COLS = [ v for (_,v) in NORMALISE.items()]
NORMALISE.update(d)
TOUTES_LES_COLS.extend(c)
TOUTES_LES_COLS.extend(["type_Terminale", "type_Première", "année_scolaireTerminale"])
    




    

#############################################################
####             Traitement des colonnes                 ####
#############################################################


# Fonction à appliquer à chaque élément de la colonne
# sera appliqué *après* normalisation du nom de la colonne

dico_appréciation={
    "Peu démontrée":0,
    "Défavorable":0,
    "Réservé":1,
    "Favorable":2,
    "Défavorable":3,
    "Moyen":0,
    "Assez bon":1,
    "Bon":2,
    "Très bon":3,
    "Assez satisfaisante":1,
    "Assez satisfaisant":1,
    "Satisfaisante":2,
    "Satisfaisant":2,
    "Très satisfaisante":3,
    "Très satisfaisant":3,
    "":1
    }

dico_sat={
    "Moyen":0,
    "Assez bon":1,
    "Bon":2,
    "Très satisfaisante":3 }
dico_boursier={
    "Non boursier":False,
    "Boursier du secondaire":True,
    "Boursier de l'enseignement supérieur":True}
    
boursier_vers_int = lambda x : int(dico_boursier[x])
appréciation_vers_int = lambda x : dico_appréciation[x]

def au_dessus_de_10(n):
    val = float_ou_rien(n)
    if val != None and val >10:
        return val-10
    else:
        return 0

def dossier_complet(x):
    return x=="ECF"

#À FAIRE
def année_davance(année):
    """ Renvoie la fonction qui à une date de naissance associe le nb d'année d'avance (éventuellement négatif)"""
    
    def res(date):
        return 0

def int_ou_rien(c):
    try : return int(c)
    except : return None
    
def float_ou_rien(c):
    try : return float(c.replace(",","."))
    except (TypeError, ValueError) : return None


    # Dictionnaire nom de la colonne -> traitement
TRAITEMENTAPB={
    "numéro_ligne":int, 
    #"nom":en_ASCII_majuscule, 
    #"prénom":en_ASCII_minuscule_Initiale,
    "année":int_ou_rien,
    "boursier":boursier_vers_int,
    "Dpt_lycée_origine":int_ou_rien,
    "entrée_seconde":int_ou_rien,
    "EAF_écrit": float_ou_rien,
    "EAF_oral":float_ou_rien,
    "numéro":int_ou_rien,
    "capacité_investissement":appréciation_vers_int,
    "autonomie":appréciation_vers_int,
    "méthode_travail":appréciation_vers_int,
    "niveau_classe":appréciation_vers_int,
    "avis_CE":appréciation_vers_int,
    "dossier_complet":dossier_complet
}

for mat in MATIÈRES:
    # matières avec plusieurs entrées
    for trimestre in range(1,4):
        for s in SUFFIXES:
            col = f"{mat}_t{trimestre}{s}"
            col2=f"{mat}_t{trimestre}_classe{s}"
            if col in TOUTES_LES_COLS:
                TRAITEMENTAPB[col] = float_ou_rien
                TRAITEMENTAPB[col2] = float_ou_rien
# Matières non traitées ci-dessus (n'apparaissent pas de la mêm, manière dans le csv).
TRAITEMENTAPB["maths_ex"] = au_dessus_de_10
TRAITEMENTAPB["philo"] = float_ou_rien
TRAITEMENTAPB["français_écrit"] = float_ou_rien
TRAITEMENTAPB["français_oral"] = float_ou_rien




###################################################
####         Nouvelles colonnes à créer:       ####
###################################################

# Attention : je pars du principe que les colonnes à créer sont de type REAL pour sqlite. Sinon, il faudra revoir la structure de la table (fonction crée_table_APB)

# Dico nom de la colonne à créer -> formule, de type (dico matière->float) -> float



def note_avec_moyenne_classe(mat, année, nb_trimestres, tol=0):
    """
    Entrées : mat, str, élément de MATIÈRES_RÉPÉTÉES
              année, str : "Première" ou "Terminale" (élément de SUFFFIXES)
    Sortie : la fonction qui au dico d d'un élève associe la moyenne dans la matière mat pour l'année année. La moyenne de la classe est prise en compte. Renvoie 0 en cas de donnée manquante. Renvoie None si > tol notes manquantes.
    """
    def f(d):
            s=0
            nb_notes=0
            for trimestre in range(nb_trimestres):
                try:
                    m = d[ f"{mat}_t{trimestre+1}_classe{année}" ]
                    x = d[ f"{mat}_t{trimestre+1}{année}" ]
                    x_modifié =  10 + (x-m)/(20-m)*10
                    s += (x+x_modifié)/2
                    nb_notes+=1
                except TypeError: # Données manquante
                    pass
                except ZeroDivisionError:
                    print(f" Une classe avait 20 de moyenne, en {mat} {année} trimestre {trimestre}. {d['nom'], d['prénom']}\n")
                    s+=20
                    nb_notes+=1
            if nb_trimestres - nb_notes > tol:
                return 0
            else: return s/nb_notes
        
    return f


COLONNES_À_CRÉER=[ ( f"moyenne_{mat}_première", note_avec_moyenne_classe(mat, "Première", 2, tol=1)) for (_,mat) in MATIÈRES_RÉPÉTÉES ]
COLONNES_À_CRÉER.extend([ ( f"moyenne_{mat}_term",  note_avec_moyenne_classe(mat, "Terminale", 2, tol=1)) for (_, mat) in MATIÈRES_RÉPÉTÉES ]) 
# L'argument facultatif mat=mat est une astuce pour forcer l'évaluation de mat au moment de la création du lambda... Cf « Python late binding closure »


def moyenne(formule, tol=0, debug=False):
    """ Prend un tuple de couple (matière, coeff) et renvoie la fonction qui prend le dico des notes et renvoie la moyenne.
    Renvoie 0 s'il manque > tol notes."""
    def f(d):
        somme, coeffs = 0, 0
        pasDeNote=[]
        for (mat, c) in formule:
            if mat in d and d[mat]!=None:
                somme += d[mat]*c
                coeffs += c
            else:
                pasDeNote.append(mat)
        if len(pasDeNote)>tol or coeffs==0:
            if debug:print(f"Attention : il m'a manqué {len(pasDeNote)} notes ({pasDeNote}) lors du traitement de la ligne : {d}\n")
            return 0
        else:
            return somme/coeffs
    return f



def moyenne_à_choix(formule, nbNotes, total_coeffs, debug=True):
    """
    Entrée : itérable de couples (matière, coeff).
             nbNotes : nb de notes minimal attendu. Renvoie None si ce nombre n'est pas atteint.
             total_coeffs : total des coeffs (ce par quoi diviser à la fin).
    Sortie : fonction qui prend le dico des notes d'un élève et renvoie la moyenne.

    Le nombre d'éléments dans la formule peut être strictement plus grand que nbNotes. La somme sera effectuée uniquement sur les notes disponibles dans d.
    """
    def f(d):
        nb_notes_trouvées=0
        somme=0
        mat_manquantes = []
        for mat, c in formule :
            if mat in d and d[mat]!=None:
                #print(mat)
                somme += d[mat]*c
                nb_notes_trouvées+=1
            else:
                mat_manquantes.append(mat)
        if nb_notes_trouvées < nbNotes:
            if debug:print(f" Il m'a manqué {nbNotes-nb_notes_trouvées} notes ({mat_manquantes}) lors du traitement de la ligne de {d['nom'], d['prénom']}.\n")
            return None
        else:
            return somme/total_coeffs
    return f



#print(COLONNES_À_CRÉER)
    
COLONNES_À_CRÉER.extend([
    ("français" ,  moyenne([ ("français_écrit",3), ("français_oral",2) ]) ),
    ("Appréciations", lambda d: (d["capacité_investissement"] + d["autonomie"] + d["méthode_travail"] + d["avis_CE"]+ d["niveau_classe"])*20/20   ),

    
    ("moyenneMP", moyenne_à_choix( [("moyenne_maths_première", 6),
                                    ("moyenne_maths_term", 18),
                                    ("moyenne_PC_première", 4),
                                    ("moyenne_PC_term", 10),
                                    ("moyenne_SI_sans_physique_première", 4),
                                    ("moyenne_SI_term", 10),
                                    ("moyenne_NSI_première", 5),
                                    ("moyenne_NSI_term", 1),
                                    ("moyenne_LV1_première", 1),
                                    ("moyenne_LV1_term", 4),                                    
                                    ("Appréciations", 2),
                                    ("maths_ex", 12),
                                    ("français", 4.5),
                                    ("philo", 4.5) ],
                                   9, 59 )), #nb notes min attendues, total des coeffs. (Maths ex pas compté dedans)


     # Moyenne classique entre toutes les notes dispo, avec une tolérance de 5, ce qui permet pas de NSI en première et une note manquante.
    ("moyenneMPPartielle", moyenne([("moyenne_maths_première", 6),
                                    ("moyenne_maths_term", 18),
                                    ("moyenne_PC_première", 4),
                                    ("moyenne_PC_term", 10),
                                    ("moyenne_SI_première", 4),
                                    ("moyenne_SI_term", 10),
                                    ("moyenne_NSI_première", 5),
                                    ("moyenne_NSI_term", 1),
                                    ("moyenne_LV1_première", 1),
                                    ("moyenne_LV1_term", 4),                                    
                                    ("Appréciations", 2),
                                    ("maths_ex", 12),
                                    ("français", 4.5),
                                    ("philo", 4.5) ],
                                   tol = 5 )), 
     
    ])

for c,_ in COLONNES_À_CRÉER:
    TOUTES_LES_COLS.append(c)

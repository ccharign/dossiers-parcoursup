# -*- coding:utf-8 -*-




# A Faire : charger tous les csv parcoursup.
# Fusion en cas de deux dossiers

from sqlite import *
from boîteÀOutils import affiche_dico
import glob
import os

from paramétrage import *

import os
ICI="/home/moi/git/dossiers parcoursup"
DONNÉES=os.path.join(ICI,"données")
BASE_BROUILLON=ICI+"/brouillon.db"
MPSI2021=os.path.join(DONNÉES,"/2021/MPSI.csv")
BASE = os.path.join(DONNÉES, "/2021/données.db")




def crée_table_APB(base, nom="Dossiers", debug=False, col_supplémentaire=None, ajout_numéro_ligne=False):
    """ Crée, ou réinitialise la table pour les dossiers parcourssup
       col_supplémentaire, si indiqué doit être de la forme '"champs" type' , prêt à être envoyé à sqlite 
       si ajout_numéro_ligne, on rajoute une glef primaire autoincrémentée appelée « numéro_ligne».
     TOUTES_LES_COLS est une variable globale, str list, avec la liste des noms de colonnes. 
 """
    
    conn = sqlite3.connect(base)
    cu = conn.cursor()
    cu.execute(f"DROP TABLE IF EXISTS {nom};")

    champs = []
    for c in TOUTES_LES_COLS :
        # Détection grossière du type
        if c in TRAITEMENTAPB :
            if TRAITEMENTAPB[c] in [int_ou_rien, boursier_vers_int, dossier_complet ]:
                champs.append( entreGuillemets(c)+" INT")
            elif TRAITEMENTAPB[c] in [float_ou_rien, au_dessus_de_10]:
                champs.append( entreGuillemets(c)+" REAL")
            else:
                champs.append( entreGuillemets(c)+" TEXT")
        elif c in [x for (x,_) in COLONNES_À_CRÉER]:
            champs.append( entreGuillemets(c)+" REAL")
        else:
            champs.append( entreGuillemets(c)+" TEXT")

    champs.append(' "année" INT')
    
    if col_supplémentaire!=None:
        champs.append(col_supplémentaire)
    if ajout_numéro_ligne:
        champs = ['"numéro_ligne" INTEGER PRIMARY KEY AUTOINCREMENT'] + champs

    # Création de la requête SQL
    champs=", ".join(champs)
    requête=f"CREATE TABLE {nom} ({champs});"
    if debug:print(requête)
    cu.execute(requête)

    conn.commit()
    cu.close()
    conn.close()

def fusion_ligne(d, conn, table, année, debug=False):
    """ Entrée : d, dictionnaire d'une ligne de données correspondant à un élève déjà présent dans la base
                 conn, connexion vers la base
                 la ligne est identifiée par d["numéro"] et année
        Effet : rajoute les nouvelles données"""
        
    numéro=d["numéro"]
    cu=conn.cursor()
    cu.execute( f"SELECT * FROM {table} WHERE numéro={numéro} AND année={année}")
    res=cu.fetchall()
    if len(res) > 1:
        raise ValueError( f"Plusieurs entrées pour le numéro {numéro}\n" )
    else:
        
        # Recherche des nouveautés
        d2=dict( res[0] ) # anciennes valeurs
        changement=False
        for c, v in d.items():
            if v==None and d2[c]!=None: # L'ancienne version avait une valeur que le nouveau n'a pas
                d[c]=d2[c]
            if d[c]!=d2[c]: # Le nouveau dico d a une modif
                if debug:print( f"Je vais compléter la ligne du numéro {numéro}, pour le champ {c} avec la valeur {d[c]}\n   (ancienne valeur : {d2[c]})\n" )
                changement=True
                
        if changement:
            # Recalculer les valeurs des colonnes rajoutées automatiquement
            for c, f  in COLONNES_À_CRÉER:
                d[c] = f(d)

            # Màj de la table
            trous = ",".join([ f"{c} = :{c}" for c in d.keys()])
            requête=f"UPDATE {table} SET {trous} WHERE numéro={numéro}"
            #print(requête,"\n")
            cu.execute(requête, d)
            
    cu.close()
    


    
def charge_parcours_sup(chemin, base, année, table="Dossiers", debug=False):
    """ Entrée : chemin, chemin vers le csv
        Base : chemin vers la base sqlite finale
        année : année
        
        
        Charge le contenu du csv dans la base.
        Attention : la colonne du classement établi lors de la commission de sélection doit s'appeler « Classement Barthou »

         Les dictionnaires suivants doivent avoir été créés en global: normalise_matière, NORMALISE, COLONNES_À_CRÉER, TRAITEMENTAPB, SUFFIXES
            
        """

    if debug:log = print
    else:log = lambda _:None

    entrée = open(chemin,"r")
    

    # Création du dictionnaire nom -> indice de col dans le csv
    # Chaque matière mat est suivie de Classement (à nommer classement_absolu_mat) puis Effectif (à nommer effectif_mat)
    indice_col = {}
    noms_cols = [] # Liste des noms de colonnes
    premièreLigne = entrée.readline().strip().split(";")
    #if debug:print(premièreLigne)
    suffixe = "" #sera rajouté en fin de chaque colonne
    for i, c in enumerate(premièreLigne):
        if c in SUFFIXES:
            suffixe=c
            log(f"Je passe à {c}")
        elif c in NORMALISE: # Autre colonne reconnue
            nom = NORMALISE[c]+suffixe
            if nom in noms_cols:
                print(f"Colonne en double : {nom}. Je garde la dernière.\n")
            else:
                noms_cols.append(nom)
            indice_col[nom] = i #Je garde la dernière colonne en cas de doublon
        else:
            log(f"Colonne ignorée : {c}")
    if debug:
       print("première ligne lue\n voici les nom des colonnes créées", noms_cols)
       affiche_dico(indice_col)
       print(len(noms_cols), " champs récupérés\n\n")



    conn=sqlite3.connect(base)
    conn.row_factory=sqlite3.Row
    curseur=conn.cursor()
    
    curseur.execute(f"SELECT numéro FROM {table} WHERE année={année};")
    numéros_présents = [ n for (n,) in curseur ]
    if debug:print("numéros déjà présents : ", numéros_présents)
    
    def ligne_vers_table(ligne):
        """Prend la ligne du csv, la transforme et la met dans la table
            ligne est de type list"""
        res={} # res sera le dictionnaire col -> valeur
        
        # 1) Traitement des cols existantes
        for c in noms_cols :
            i=indice_col[c]
            if c in TRAITEMENTAPB :
                res[c] = TRAITEMENTAPB[c](ligne[i])
            else:
                res[c]=ligne[i]

        #2) Création des nouvelles colonnes
        for c, f  in COLONNES_À_CRÉER:
            
            res[c] = f(res)
        res["année"]=année
        
        #3) Envoi vers la table
        if res["numéro"] not in numéros_présents:
            dico_vers_table(res, table,curseur)
        else:
            fusion_ligne(res, conn, table, année, debug=debug)


    for ligne in entrée:
        ligne_vers_table(ligne.strip().split(";"))
    conn.commit()
    curseur.close()
    conn.close()


def charge_dossier_une_année(rép, base=BASE_BROUILLON, debug=False):
    """ Le répertoire a un nom d'année. Il a un sous-répertoire "dossiers"."""
    année = re.search("20[0123456789]{2}", rép)
    if année:
        année = int(année.group())
        print("année détectée : ", année,"\n")
    else:
        print("Ce répertoire n'a pas un nom d'année : ", rép,"\n")
    l=glob.glob(rép+'/dossiers/*')
    for f in l:
        if "APB" in f or "ParcourSup" in f:
            print(f"Lecture du fichier {f}")
            charge_parcours_sup(f, base, année, debug=debug)


def charge_tous_les_dossier(rép, base=BASE_BROUILLON, debug=False):
    l=glob.glob(rép+'/*')
    for f in l:
        if os.path.isdir(f):
            print("\n"+f)
            charge_dossier_une_année(f, base=base, debug=debug)

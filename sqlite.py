﻿#! /usr/bin/python3
# -*- coding: utf-8 -*-

import sqlite3
import re #expressions régulières

# ajout du 30/10/18 :
#   - nouvelle table Classes qui indique la filière correspondant à chaque classe
#   - le champ MEF de la table Parcours est rempli avec la filière correspondant à la classe


def execute_fichier_sql(chemin, curseur):
    entrée=open(chemin)
    requêtes = entrée.read().split(";")
    entrée.close()
    for r in requêtes:
            curseur.execute(r)

def execute_str_sql(chaine, curseur):
    requêtes = chaine.split(";")
    for r in requêtes:
            curseur.execute(r)


# Pompé sur stackoverflow
def multiple_replace(dico, text):
  # Create a regular expression  from the dictionary keys
  regex = re.compile("(%s)" % "|".join(map(re.escape, dico.keys())))

  # For each match, look-up corresponding value in dictionary
  return regex.sub(lambda mo: dico[mo.string[mo.start():mo.end()]], text) 


def en_ASCII_majuscule(c):
    d={ "é":"e", "è":"e", "ê":"e", "à":"a", "ù":"u", "ï":"i", "ô":"o", "ö":"o", "--":" " }# Les caractères spéciaux pas dans ce dico seront supprimés
    c1=multiple_replace(d,c)
    #On passe par le type «bytes»... 
    return c1.encode("ASCII","ignore").upper().decode("ASCII")

def en_ASCII_minuscule_Initiale(c):
    d={ "é":"e", "è":"e", "ê":"e", "à":"a", "ù":"u", "ï":"i", "ô":"o", "ö":"o", "--":" "}
    c1=multiple_replace(d,c)
    return c1.encode("ASCII","ignore").decode("ASCII").capitalize()


# Dictionnaire des traitements à appliquer par défaut
TRAITEMENT={
    "nom":en_ASCII_majuscule, # Les noms sont sans accent et en majuscule
    "prénom":en_ASCII_minuscule_Initiale}



def executeRequete(r, c):
    """ Le seul but de cette procédure est d'afficher la requête avant de l'exécuter."""
    print(r+"\n")
    c.execute(r)

def trous(n,symbole="?"):
    """ Renvoie une chaîne contenant n fois le symbole, séparés par des virgules, entourés par des parenthèses."""
    assert n>0
    return "(" + (symbole+", ")*(n-1) + symbole + ")"

def entreGuillemets(c):
    return '"'+c+'"'

def simplifieChaîne(c):
    """ Remplace les caractères pouvant poser problème dans un nom de fichier"""
    return re.sub("/" , "-",str(c))
    

def csv_vers_table(chemin_csv, conn, nom, sep=';', requête_création=None):
    """ nom est le nom de la table à créer.
        requête_création est la requête à utiliser pour créer la table (utile pour préciser le type).
    Attention : si elle existait déjà, elle sera vidée."""
    csv=open(chemin_csv, 'r')
    champs=tuple(csv.readline().strip().split(sep))
    n=len(champs)

    c=conn.cursor()
    executeRequete("DROP TABLE IF EXISTS {};".format(nom),c)
    if requête_création==None:
        executeRequete( "CREATE TABLE {} {}".format(nom, champs),c)
    else:
        executeRequete(requête_création,c)

    for ligne in csv:
        donnees=tuple(ligne.strip().split(sep))
        requete = "INSERT INTO {} VALUES {}".format(nom, trous(n))
        c.execute(requete, donnees)
        
    conn.commit()
    c.close()
    csv.close()






def récupère_champs(curseur):
    """
    curseur est un curseur qui vient d'effectuer un SELECT.
    Ceci renvoie la liste des champs obtenus par cette recherche."""
    return [ description[0] for description in curseur.description  ]

def écrire_tuple(t, fichier):
    """ Ecrit un tuple dans un fichier csv. Par rapport à un simple fichier.write(str(t)), il s'agit en gros d'enlever les parenthèses, de mettre des point-virgules au lieu des virgule, et de rajouter un retour-chariot."""
    fichier.write(";".join([str(c) for c in list(t)])+"\n")
    

def curseur_vers_csv(curseur, chemin_csv):
    f=open(chemin_csv, 'w')
    écrire_tuple( récupère_champs(curseur), f)

    for ligne in curseur:
        écrire_tuple(ligne, f)
    f.close()
    


def curseur_vers_table(curseur, conn, table, création=False):
    """ entrée :
        - curseur : curseur qui vient d'effectuer une recherche
        - conn : connexion à une base sqlite
        - table : nom de la table à remplir

    Transfère le résultat de la recherche dans la table. Si création est vrai, la table sera crée, ou vidée si elle existait déjà.
        """

    c=conn.cursor()
    champs=tuple(récupère_champs(curseur))
    n=len(champs)
    
    if création:
        executeRequete("DROP TABLE IF EXISTS {};".format(table),c)
        executeRequete( "CREATE TABLE {} {}".format(table, champs),c)
    else:
        executeRequete( "CREATE TABLE IF NOT EXISTS {} {}".format(table, champs),c)

    for ligne in curseur:
        requete = "INSERT INTO {} VALUES {}".format(table, trous(n))
        c.execute(requete, ligne)

    conn.commit()
    c.close()


def dico_vers_table(d, table, curseur, debug=False):
    """ Entrée : - curseur, curseur vers une base
                 - d, dictionnaire nom_cololnne -> valeur
                 - table : nom d'un table de la base
        Effet : insère la ligne correspondant à d dans la table.
        Le changement n'est *pas* commité"""
    cols=",".join(d.keys())
    trous =":"+ ", :".join(d.keys())
    requête=f"INSERT INTO {table} ({cols}) VALUES ({trous})"
    curseur.execute(requête, d)






